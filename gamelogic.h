#ifndef GAMELOGIC_H
#define GAMELOGIC_H

#include <QAbstractTableModel>
#include <QStringListModel>
#include <vector>
#include <QUrl>

class GameLogic : public QAbstractListModel
{
    Q_OBJECT
public:
    explicit GameLogic(QObject *parent = nullptr);
    GameLogic(GameLogic &&object);
     int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    Q_INVOKABLE int getColumnCount() const;
    Q_INVOKABLE int getRowCount() const;
    Q_INVOKABLE int cellsCount() const;
    Q_INVOKABLE bool cellsCompareValues();
    Q_INVOKABLE void setCellOne(int index);
    Q_INVOKABLE void setCellTwo(int index);
    Q_INVOKABLE void setScore(int score);
    Q_INVOKABLE int getScore() const;
    Q_INVOKABLE void newGame();

    Q_INVOKABLE QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
    Q_INVOKABLE QVariant cell(int index) const;
    Q_PROPERTY (int score READ getScore WRITE setScore NOTIFY scoreWasChanged)
signals:
    void scoreWasChanged(int score);
protected:
    std::vector<QString> cellsData;
    int pairCount;
    int cellOne;
    int cellTwo;
    int m_score;

};

#endif // GAMELOGIC_H
