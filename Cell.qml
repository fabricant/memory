import QtQuick 2.12

Image {
    property string cellPath: "qrc:/images/cell.jpg"
    property string imagePath: ""
    property int cellIndex: 0
    readonly property string cellState: "cell"
    readonly property string imageState: "image"

    signal cellClicked(int index)
    function setCellState() {
        state = cellState
    }

    function setImageState() {
        state = imageState
    }
    id: cell
    source: cellPath
    state:cellState
    transform: Rotation {
        id: rotate

        axis {
            x: 0;
            y: 1;
            z: 0
        }
        origin.x: cell.width / 2

    }


    states: [
        State {
            name: cellState
            PropertyChanges {
                target: rotate
                angle:0
            }

            PropertyChanges {
                target: cell
                source:cellPath
                fillMode: Image.Stretch
            }
        },
        State {
            name: imageState
            PropertyChanges {
                target: rotate
                angle:180
            }
            PropertyChanges {
                target: cell
                source:imagePath
                fillMode: Image.PreserveAspectFit
            }
        }
    ]

    transitions: [
        Transition {
            from: cellState
            to: imageState
            NumberAnimation {
                target: rotate
                duration: 1000
                properties: "angle"
                easing.type: Easing.InOutSine
            }

            PropertyAnimation {
                target: cell
                duration: 500
                properties: "source"
            }

            PropertyAnimation {
                target: cell
                duration: 500
                properties: "fillMode"
            }
        },

        Transition {
            from: imageState
            to: cellState
            NumberAnimation {
                target: rotate
                duration: 1000
                properties: "angle"
                easing.type: Easing.InOutSine
            }
            SequentialAnimation {
                PropertyAnimation {
                    target: cell
                    duration: 500
                    properties: "source"
                }

                PropertyAction {
                    target: cell
                    properties: "fillMode"
                }
            }
        }
    ]

    MouseArea {
        anchors.fill: parent
        onClicked: {
            switch (parent.state){
            case cellState:
                cellClicked(cellIndex)
                break

            default:
                break;
            }

        }
    }
}


