#include "gamelogic.h"
#include "constants.h"
#include <sstream>
#include <random>
#include <algorithm>
#include <QJsonObject>
#include <QPair>

using namespace std;
GameLogic::GameLogic(QObject *parent) : QAbstractListModel(parent)
{
    cellsData.reserve(cellsCountValue);
    for (unsigned short i = 0; i < cellsCountValue; ++i) {
        QString string = QString(photoUrl).arg(i/2);

        cellsData.push_back(string);

    }
    newGame();
}

GameLogic::GameLogic(GameLogic &&object)
{
    cellsData = move(object.cellsData);
    pairCount = object.pairCount;
    cellOne = object.cellOne;
    cellTwo = object.cellTwo;
}

int GameLogic::rowCount(const QModelIndex &) const
{
    return cellsData.size();
}

int GameLogic::getColumnCount() const
{
    return columnCountValue;
}

int GameLogic::getRowCount() const
{
    return rowCountValue;
}

int GameLogic::cellsCount() const
{
    return cellsData.size();
}

bool GameLogic::cellsCompareValues()
{
    if(cellOne == noCell || cellTwo == noCell) {
        return false;
    }

    setScore(m_score + 1);

    if(!cellsData[cellOne].compare(cellsData[cellTwo])) {
        --pairCount;
        cellOne = noCell;
        cellTwo = noCell;
        return true;
    }

    return false;
}

void GameLogic::setCellOne(int index)
{
    cellOne = index;

}

void GameLogic::setCellTwo(int index)
{
    cellTwo = index;
}

void GameLogic::setScore(int score)
{
    m_score = score;
    emit scoreWasChanged(score);

}

int GameLogic::getScore() const
{
    return m_score;
}

QVariant GameLogic::data(const QModelIndex &index, int ) const
{
    if (!index.isValid()) {
        return QVariant();
    }
    if (index.row() < 0 || index.row() >= cellsData.size()) {
        return QVariant();
    }
    QJsonObject obj;
    obj["modelData"] = cellsData[index.row() * index.column()];
    return cellsData[index.row()];
}

QVariant GameLogic::cell(int index) const
{
    return cellsData[index];
}

void GameLogic::newGame()
{
    pairCount = cellsCountValue / 2;
    setScore(0);
    std::random_device rd;
    std::mt19937 g(rd());
    beginResetModel();
    shuffle(cellsData.begin(), cellsData.end(), g);
    endResetModel();
}
