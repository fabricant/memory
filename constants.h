#ifndef CONSTANTS_H
#define CONSTANTS_H
const unsigned short rowCountValue = 6;
const unsigned short columnCountValue = 6;
constexpr int cellsCountValue = rowCountValue * columnCountValue;
const char *photoUrl = "qrc:/images/Flower_%1.png";
const int noCell = -1;
#endif // CONSTANTS_H
