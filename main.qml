import QtQuick 2.8
import QtQuick.Window 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.12
import com.finc.GameLogic 1.0

Window {
    property int rowCount: gameLogic.getRowCount()
    property int columnCount: gameLogic.getColumnCount()
    property string str
    visible: true
    width: 640
    height: 480
    objectName: "mainWindow"
    title: qsTr(str)

    GameLogic {
        id:gameLogic

    }


    GridView {
        id: gView
        model: gameLogic
        anchors.top: parent.top
        anchors.bottom: textField.top
        anchors.left: parent.left
        anchors.right: parent.right
        delegate: delegate
        cellHeight: height / rowCount
        cellWidth: width / columnCount
        property int openedCell: 0
        property Cell cellOne
        property Cell cellTwo

        Timer {
            id:resetTimer
            interval: 3000; running: false; repeat: false
            onTriggered: {
                gView.cellOne.setCellState()
                gView.cellTwo.setCellState()
                gView.openedCell = 0
            }
        }

        function verifyCell (cell) {
            if (!openedCell) {
                cellOne = cell
                cellOne.setImageState()
                gameLogic.setCellOne(cell.cellIndex)
                ++gView.openedCell
            } else if (openedCell == 1) {
                cellTwo = cell
                cellTwo.setImageState()
                gameLogic.setCellTwo(cell.cellIndex)
                ++gView.openedCell

                if (!gameLogic.cellsCompareValues()){
                    resetTimer.start()
                }
                else {
                    openedCell = 0
                }

            }
        }
    }

    Button {
        id: textField
        text: gameLogic.score
        anchors.bottom: parent.bottom

        width: parent.width / 2
        height: parent.height / 10;
        font.pointSize: (height)?height * .3 : 8
//        verticalAlignment: Text.AlignVCenter
//        horizontalAlignment: Text.AlignHCenter
    }

    Button {
        anchors.top:gView.bottom
        anchors.right: parent.right
        anchors.left: textField.right
        anchors.bottom: parent.bottom
        font.pointSize: textField.font.pointSize
        text: "New Game"
        onClicked: {
            resetTimer.stop()
            gView.openedCell = 0
            gameLogic.newGame()
        }
    }

    Component {
        id:delegate
        Cell {
            id: name
            imagePath: display
            width:gView.cellWidth
            height: gView.cellHeight
            cellIndex: index
            onCellClicked: {
                gView.verifyCell(this)
            }

        }
    }
}


